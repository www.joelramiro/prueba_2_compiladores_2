.data
    str1:  .asciiz "Ingrese palabra (10 limite):"
    str2:  .asciiz "\nNumero de vocales:\n"
    name:  .asciiz ""

.text

.globl main

main:
    li $t1,11 # tamano del string

    la $a0, str1
    li $v0, 4
    syscall

    la $a0,name
    move $a1,$t1
    li $v0,8
    syscall

    li $t0,0 #numero de vocales
    li $t2,0 #contador

    j for
    end_for:

            
            
            la $a0, str2
            li $v0, 4
            syscall

            move $a0,$t0
            li $v0,1
            syscall

    li $v0,10
    syscall


for:
    bge $t2,$t1,end_for
        la $t6,name #5000
        add $t5,$t2,$t6 #5000+1,+2,+3...5001,5002
        lb $t3,($t5) # obtengo el valor ascii, a =97,b = 98....
        
        li $t4,97 #a
        beq $t3,$t4,contar

        li $t4,101 #e
        beq $t3,$t4,contar
        
        li $t4,105 #i
        beq $t3,$t4,contar
        
        li $t4,111 #o
        beq $t3,$t4,contar
        
        li $t4,117 #u
        beq $t3,$t4,contar

        j fin_contar
        contar:
            addi $t0,$t0,1 #aumenta el numero de vocales
            
        fin_contar:
        addi $t2,$t2,1 # aumenta el contador en 1
        j for



